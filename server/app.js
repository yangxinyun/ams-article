let express = require('express')
let bodyParser = require('body-parser')
let router = require('./router')

let app = express()

app.all('*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1')
  res.header("Content-Type", "application/json;charset=utf-8");
  next();
});



app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())



app.use(router)




app.listen(3500, (err) => {
  if (err)
    throw err;
  console.log('running in 3500');
})
